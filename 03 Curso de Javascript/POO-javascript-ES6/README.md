**_proyecto de js orientado a objetos_**

## instalaciones y configuraciones

- npm --init
  inicializa un proyecto de js y básicamente crea un package.json y
  la carpeta node_modules para las dependencias

- npm install -g typescript
  instala globalmente tyepscript, ya lo puedes usar en cualquier proyecto

- tsc --init
  crea el fichero de configuración para ese proyecto llamado tsconfig.json
  en él hemos hecho las siguientes modificaciones:

  - "watch": true // está pendiente de cambios sin tener que recompilar manualmente
  - "outFiles": ["./app/app.js"] // compilados aquí
  - "files": ["./app/app.ts"] // desde dónde compilará

- tsc
  arrabca el compilador de typescript según las configuraciones anteriores

## ¿Cómo proceder?

Para ejecutar un proyecto ingrese en la carpeta enumerada 01, 02, 03, ...
ejecute el **index.html** desde un navegador
yo utilizo VSC y la extensión **LiveServer** que ejecuta el proyecto y está pendiente de los cambios
que se producen en él.

** typescript está basado en este caso en **ES5**, se puede configurar para ES6 (con más funcionalidades)
** pero para estos ejemplos no es necesario. Si fuésemos a programar con Node.js o con Angular si sería adecuado para implemenatr 'arrow functions', 'promesas', 'async await', 'tipado fuerte', Rxjs, etc.

- Typescript es como un js con anabolizantes
- los cambios en **app.ts** son detectados por typescript y compilados en **app.js**
- para ello debes ejecutar el comando de consola tsc
- una vez se ejecuta **tsc** se encarga de compilar cada vez que grabamos el archivo app.ts
- en un proyecto real, la carpeta app, subirá a la rama correspondiente, pero cuando
  se haga un **build** de la misma, será solo compilado los **.js**

## cada carpeta es independiente

- _01 POO:_ presenta mis conocimientos básicos sobre clases y POO en js
- _02 POO ADVANCED:_ añadiendo algo más de complejidad
-
