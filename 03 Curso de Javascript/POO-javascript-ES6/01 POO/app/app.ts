class Poligono {
	protected numeroLados: number;
	protected medidaLado: number;

	constructor(numero_lados: number, medida_lado: number) {
		this.numeroLados = numero_lados;
		this.medidaLado = medida_lado;
	}

	public print() {
		console.log(`número lados: ${this.numeroLados}, medida del lado: ${this.medidaLado}`);
	}

	public perimetro() {
		return this.numeroLados * this.medidaLado;
	}

	static whoami() {
		return 'Soy un polígono';
	}
}

class Cuadrado extends Poligono {
	private color: string;

	constructor(medida_lado: number, color: string) {
		super(4, medida_lado);
		this.color = color;
	}
	public get getColor() {
		return this.color;
	}
	public set setColor(nuevo_color: string) {
		this.color = nuevo_color;
	}
	public perimetro() {
		return super.perimetro();
	}
	public print() {
		console.log(`número lados: 4, medida del lado: ${this.medidaLado}, color: ${this.color}`);
	}

	static whoami() {
		return 'Soy un polígono, de 4 lados';
	}

	public area() {
		return `${this.medidaLado * this.medidaLado} cm2`;
	}
}

class Pentagono extends Poligono {
	private color: string;
	static coeficiente: number = 1.72;

	constructor(medida_lado: number, color: string) {
		super(5, medida_lado);
		this.color = color;
	}
	public get getColor() {
		return this.color;
	}
	public set setColor(nuevo_color: string) {
		this.color = nuevo_color;
	}
	public get getCoeficiente() {
		return Pentagono.coeficiente;
	}
	public perimetro() {
		return super.perimetro();
	}
	public print() {
		console.log(`número lados: 5, medida del lado: ${this.medidaLado}, color: ${this.color}`);
	}

	static whoami() {
		return 'Soy un polígono, de 5 lados, o sea, un pentágono';
	}

	public area() {
		return `${this.medidaLado * this.medidaLado * this.getCoeficiente} cm2`;
	}
}

// instanciamos Poligono y usamos sus métodos
let poligono1 = new Poligono(3, 8);
poligono1.print(); // "número lados: 3, medida: 8"
poligono1.perimetro(); // 24
Poligono.whoami(); // "Soy un polígono"

// instanciamos Cuadrado y usamos sus métodos
let cuadrado1 = new Cuadrado(4, 'rosa');
cuadrado1.print(); // "número lados: 4, medida: 4, color: rosa"
cuadrado1.perimetro(); // 16
Cuadrado.whoami(); // "Soy un polígono, de tipo cuadrado"
cuadrado1.area(); // "16 cm2"
cuadrado1.getColor; // "rosa" // los métodos getter and setters son propiedades para js
cuadrado1.setColor = 'verde'; // los métodos getter and setters son propiedades para js

let pentagono1 = new Pentagono(3, 'azul');
pentagono1.print(); // número lados: 5, medida: 3, color: azul
console.log(pentagono1.perimetro()); // 15
console.log(Pentagono.whoami()); // 'Soy un polígono, de 5 lados, osea un pentágono'
console.log(pentagono1.area()); // "15.48 cm2"
console.log(pentagono1.getColor); // obtenemos esa propiedad // "azul"
pentagono1.setColor = 'amarillo'; // reestablecemos la propiedad color
console.log(pentagono1.getColor); // los métodos getter and setters son propiedades para js
console.log(pentagono1.getCoeficiente); // los métodos getter and setters son propiedades para js
console.log(Pentagono.coeficiente); // los métodos y propiedades satic son accesibles desde la clase
pentagono1.print(); // número lados: 5, medida: 3, color: amarillo

// resolucion de algoritmos a través de objetos
class Solution {
	// buscar el menor entero positivo dentro de un array
	solution: number;
	constructor(A: number[]) {
		console.log('Nuevo Array creado in solution');
		console.log(A);
		console.log('the minor integer of this array is: ...');
		this.solution = this.Solution(A);
		console.log(this.solution);
	}
	public Solution(A: number[]): number {
		// write your code in Java SE 8
		let candidate = 0; // solucion
		let minorInt = 0;
		let finish = false;

		for (let i = 0; i < A.length; i++) {
			minorInt += 1; // serie begins in 1, 2, 3, 4, ...since length of A
			// console.log(`i : ${i} | minorInt: ${minorInt}`);// debug
			for (let j = 0; j < A.length; j++) {
				// console.log(`j : ${j} | minorInt: ${minorInt}`);// debug
				let counter = 0;
				if (A[i] == minorInt) {
					// all is ok we continue
					candidate = minorInt; // guard the candidate
					// console.log(`encuentra el numero en la serie, posición: ${j}, candidate: ${candidate}`); // debug
					j = A.length;
				} else {
					counter += 1;
					if (counter == A.length) {
						candidate = minorInt;
						finish = true;
						// console.log(`no lo encontró el numero en la serie, candidate = ${candidate}`); // debug
						j = A.length;
						i = A.length; // salimos con esto
					}
				}
			}
		}
		if (finish == true) {
			return candidate;
		} else {
			return candidate + 1;
		}
	}
}
var ArrayText1 = [1, 1, 3, 1, 1, 3, 2, 2, 5, -2311, 5, 6, 7, 8, 9, 0, -11, 1100]; // should return 4
const example1 = new Solution(ArrayText1);

var ArrayText2 = [-1, -1, -3, -1, -1, -3, -2, -2, -5, -2311, -5, -6, -7, -8, -9, 0, -11, 1100]; // should return 1
const example2 = new Solution(ArrayText2);
