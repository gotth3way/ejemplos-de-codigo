"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Poligono = /** @class */ (function () {
    function Poligono(numero_lados, medida_lado) {
        this.numeroLados = numero_lados;
        this.medidaLado = medida_lado;
    }
    Poligono.prototype.print = function () {
        console.log("n\u00FAmero lados: " + this.numeroLados + ", medida del lado: " + this.medidaLado);
    };
    Poligono.prototype.perimetro = function () {
        return this.numeroLados * this.medidaLado;
    };
    Poligono.whoami = function () {
        return 'Soy un polígono';
    };
    return Poligono;
}());
var Cuadrado = /** @class */ (function (_super) {
    __extends(Cuadrado, _super);
    function Cuadrado(medida_lado, color) {
        var _this = _super.call(this, 4, medida_lado) || this;
        _this.color = color;
        return _this;
    }
    Object.defineProperty(Cuadrado.prototype, "getColor", {
        get: function () {
            return this.color;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Cuadrado.prototype, "setColor", {
        set: function (nuevo_color) {
            this.color = nuevo_color;
        },
        enumerable: false,
        configurable: true
    });
    Cuadrado.prototype.perimetro = function () {
        return _super.prototype.perimetro.call(this);
    };
    Cuadrado.prototype.print = function () {
        console.log("n\u00FAmero lados: 4, medida del lado: " + this.medidaLado + ", color: " + this.color);
    };
    Cuadrado.whoami = function () {
        return 'Soy un polígono, de 4 lados';
    };
    Cuadrado.prototype.area = function () {
        return this.medidaLado * this.medidaLado + " cm2";
    };
    return Cuadrado;
}(Poligono));
var Pentagono = /** @class */ (function (_super) {
    __extends(Pentagono, _super);
    function Pentagono(medida_lado, color) {
        var _this = _super.call(this, 5, medida_lado) || this;
        _this.color = color;
        return _this;
    }
    Object.defineProperty(Pentagono.prototype, "getColor", {
        get: function () {
            return this.color;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pentagono.prototype, "setColor", {
        set: function (nuevo_color) {
            this.color = nuevo_color;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Pentagono.prototype, "getCoeficiente", {
        get: function () {
            return Pentagono.coeficiente;
        },
        enumerable: false,
        configurable: true
    });
    Pentagono.prototype.perimetro = function () {
        return _super.prototype.perimetro.call(this);
    };
    Pentagono.prototype.print = function () {
        console.log("n\u00FAmero lados: 5, medida del lado: " + this.medidaLado + ", color: " + this.color);
    };
    Pentagono.whoami = function () {
        return 'Soy un polígono, de 5 lados, o sea, un pentágono';
    };
    Pentagono.prototype.area = function () {
        return this.medidaLado * this.medidaLado * this.getCoeficiente + " cm2";
    };
    Pentagono.coeficiente = 1.72;
    return Pentagono;
}(Poligono));
// instanciamos Poligono y usamos sus métodos
var poligono1 = new Poligono(3, 8);
poligono1.print(); // "número lados: 3, medida: 8"
poligono1.perimetro(); // 24
Poligono.whoami(); // "Soy un polígono"
// instanciamos Cuadrado y usamos sus métodos
var cuadrado1 = new Cuadrado(4, 'rosa');
cuadrado1.print(); // "número lados: 4, medida: 4, color: rosa"
cuadrado1.perimetro(); // 16
Cuadrado.whoami(); // "Soy un polígono, de tipo cuadrado"
cuadrado1.area(); // "16 cm2"
cuadrado1.getColor; // "rosa" // los métodos getter and setters son propiedades para js
cuadrado1.setColor = 'verde'; // los métodos getter and setters son propiedades para js
var pentagono1 = new Pentagono(3, 'azul');
pentagono1.print(); // número lados: 5, medida: 3, color: azul
console.log(pentagono1.perimetro()); // 15
console.log(Pentagono.whoami()); // 'Soy un polígono, de 5 lados, osea un pentágono'
console.log(pentagono1.area()); // "15.48 cm2"
console.log(pentagono1.getColor); // obtenemos esa propiedad // "azul"
pentagono1.setColor = 'amarillo'; // reestablecemos la propiedad color
console.log(pentagono1.getColor); // los métodos getter and setters son propiedades para js
console.log(pentagono1.getCoeficiente); // los métodos getter and setters son propiedades para js
console.log(Pentagono.coeficiente); // los métodos y propiedades satic son accesibles desde la clase
pentagono1.print(); // número lados: 5, medida: 3, color: amarillo
// resolucion de algoritmos a través de objetos
var Solution = /** @class */ (function () {
    function Solution(A) {
        console.log('Nuevo Array creado in solution');
        console.log(A);
        console.log('the minor integer of this array is: ...');
        this.solution = this.Solution(A);
        console.log(this.solution);
    }
    Solution.prototype.Solution = function (A) {
        // write your code in Java SE 8
        var candidate = 0; // solucion
        var minorInt = 0;
        var finish = false;
        for (var i = 0; i < A.length; i++) {
            minorInt += 1; // serie begins in 1, 2, 3, 4, ...since length of A
            // console.log(`i : ${i} | minorInt: ${minorInt}`);// debug
            for (var j = 0; j < A.length; j++) {
                // console.log(`j : ${j} | minorInt: ${minorInt}`);// debug
                var counter = 0;
                if (A[i] == minorInt) {
                    // all is ok we continue
                    candidate = minorInt; // guard the candidate
                    // console.log(`encuentra el numero en la serie, posición: ${j}, candidate: ${candidate}`); // debug
                    j = A.length;
                }
                else {
                    counter += 1;
                    if (counter == A.length) {
                        candidate = minorInt;
                        finish = true;
                        // console.log(`no lo encontró el numero en la serie, candidate = ${candidate}`); // debug
                        j = A.length;
                        i = A.length; // salimos con esto
                    }
                }
            }
        }
        if (finish == true) {
            return candidate;
        }
        else {
            return candidate + 1;
        }
    };
    return Solution;
}());
var ArrayText1 = [1, 1, 3, 1, 1, 3, 2, 2, 5, -2311, 5, 6, 7, 8, 9, 0, -11, 1100]; // should return 4
var example1 = new Solution(ArrayText1);
var ArrayText2 = [-1, -1, -3, -1, -1, -3, -2, -2, -5, -2311, -5, -6, -7, -8, -9, 0, -11, 1100]; // should return 1
var example2 = new Solution(ArrayText2);
