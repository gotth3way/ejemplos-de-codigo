"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Character = /** @class */ (function () {
    function Character(mass) {
        this.mass = mass;
        this.id = Math.random() * (10000 - 1) + 1;
    }
    Character.whoami = function () {
        console.log('I`m a character of the Universe');
    };
    Character.prototype.presentation = function () {
        console.log('My Id is: ' + this.id);
        console.log('My mass is: ' + this.mass + 'Kg.');
    };
    return Character;
}());
var Soldier = /** @class */ (function (_super) {
    __extends(Soldier, _super);
    function Soldier(mass, name, velocity, hability) {
        var _this = _super.call(this, mass) || this;
        _this.name = name;
        _this.velocity = velocity;
        _this.hability = hability;
        _this.landa = _this.getHabilityPoints(hability);
        _this.power = _this.mass * _this.velocity * _this.landa;
        _this.presentation();
        return _this;
    }
    Soldier.prototype.getHabilityPoints = function (hability) {
        var habilities = {
            wizard: 1,
            resurreption: 2,
            powerfull: 3,
            taichilover: 4,
        };
        // i know that this is not acceptable, but i dont find the solution in Nodejs
        // is simple only you have todo that: habilities[hability] but in tsc this solution throw and error
        if (hability == 'wizard')
            return habilities.wizard;
        if (hability == 'resurreption')
            return habilities.resurreption;
        if (hability == 'powerfull')
            return habilities.powerfull;
        if (hability == 'taichilover')
            return habilities.taichilover;
        return 0;
    };
    Soldier.prototype.attack = function () {
        console.log('##################### ' + Soldier.type + ' attack!!');
        console.log(this.name + ' attacks with ' + this.power);
    };
    Soldier.prototype.presentation = function () {
        _super.prototype.presentation.call(this);
        console.log('Mi nombre es ' + this.name);
        console.log('Mi power is ' + this.power);
    };
    Soldier.type = 'soldier';
    return Soldier;
}(Character));
var Humanoide = /** @class */ (function (_super) {
    __extends(Humanoide, _super);
    function Humanoide(mass, name, velocity, hability) {
        var _this = _super.call(this, mass) || this;
        _this.name = name;
        _this.velocity = velocity;
        _this.hability = hability;
        _this.landa = _this.getHabilityPoints(hability);
        _this.power = _this.mass * _this.velocity * _this.landa;
        _this.presentation();
        return _this;
    }
    Humanoide.prototype.getHabilityPoints = function (hability) {
        var habilities = {
            joker: 1,
            loving: 1,
            feminist: 4,
            seriesTVlover: 4,
        };
        // i know that this is not acceptable, but i dont find the solution in Nodejs
        // is simple only you have todo that: habilities[hability] but in tsc this solution throw and error
        if (hability == 'joker')
            return habilities.joker;
        if (hability == 'loving')
            return habilities.loving;
        if (hability == 'feminist')
            return habilities.feminist;
        if (hability == 'seriesTVlover')
            return habilities.seriesTVlover;
        return 0;
    };
    Humanoide.prototype.attack = function () {
        console.log('##################### ' + Humanoide.type + ' attack!!');
        console.log(this.name + ' attacks with ' + this.power);
    };
    Humanoide.prototype.presentation = function () {
        _super.prototype.presentation.call(this);
        console.log('Mi nombre es ' + this.name);
        console.log('Mi power is ' + this.power);
    };
    Humanoide.type = 'humanoide';
    return Humanoide;
}(Character));
var Battle = /** @class */ (function () {
    function Battle(character1, character2) {
        this.character1 = character1;
        this.character2 = character2;
        this.winner = null;
        this.losser = null;
    }
    Battle.prototype.whoAttacks = function () {
        return Math.floor(Math.random() * 2 + 1);
    };
    Battle.prototype.LetsGo = function () {
        while (this.character1.power > 0 && this.character2.power > 0) {
            var turno = this.whoAttacks();
            console.log(turno);
            if (turno == 1) {
                this.character1.attack();
                this.character2.power -= this.character1.power;
            }
            else if (turno == 2) {
                this.character2.attack();
                this.character1.power -= this.character2.power;
            }
            else {
                console.log('revisa tu codigo');
            }
            console.log(this.character1.name +
                ' --> ' +
                this.character1.power +
                '|' +
                this.character2.power +
                ' <-- ' +
                this.character2.name);
        }
        if (this.character1.power > this.character2.power) {
            this.winner = this.character1;
            this.losser = this.character2;
        }
        else {
            this.winner = this.character2;
            this.losser = this.character1;
        }
        console.log('-----------------------------------------------');
        console.log('and the winner is ...' + this.winner.name);
        console.log('with ' + this.winner.power + ' points of power');
        console.log('-----------------------------------------------');
        console.log('and the second is ...' + this.losser.name);
        console.log('with ' + this.losser.power + ' points of power');
        console.log('-----------------------------------------------');
    };
    return Battle;
}());
var Game = /** @class */ (function () {
    function Game() {
        this.soldier = new Soldier(100, 'PapiChulo', 13, 'powerfull');
        this.humanoide = new Humanoide(80, 'Capuleto', 54, 'loving');
        this.battle = new Battle(this.soldier, this.humanoide);
        this.battle.LetsGo();
    }
    return Game;
}());
var game = new Game();
