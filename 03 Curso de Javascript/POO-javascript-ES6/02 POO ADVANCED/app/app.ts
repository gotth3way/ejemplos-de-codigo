class Character {
	mass: number;
	id: number;

	constructor(mass: number) {
		this.mass = mass;
		this.id = Math.random() * (10000 - 1) + 1;
	}
	static whoami() {
		console.log('I`m a character of the Universe');
	}
	presentation() {
		console.log('My Id is: ' + this.id);
		console.log('My mass is: ' + this.mass + 'Kg.');
	}
}

class Soldier extends Character {
	name: string;
	velocity: number;
	hability: string;
	power: number;
	landa: number;
	static type: string = 'soldier';

	constructor(mass: number, name: string, velocity: number, hability: string) {
		super(mass);

		this.name = name;
		this.velocity = velocity;
		this.hability = hability;
		this.landa = this.getHabilityPoints(hability);
		this.power = this.mass * this.velocity * this.landa;

		this.presentation();
	}

	getHabilityPoints(hability: string): number {
		const habilities = {
			wizard: 1,
			resurreption: 2,
			powerfull: 3,
			taichilover: 4,
		};
		// i know that this is not acceptable, but i dont find the solution in Nodejs
		// is simple only you have todo that: habilities[hability] but in tsc this solution throw and error
		if (hability == 'wizard') return habilities.wizard;
		if (hability == 'resurreption') return habilities.resurreption;
		if (hability == 'powerfull') return habilities.powerfull;
		if (hability == 'taichilover') return habilities.taichilover;
		return 0;
	}
	attack() {
		console.log('##################### ' + Soldier.type + ' attack!!');
		console.log(this.name + ' attacks with ' + this.power);
	}
	presentation() {
		super.presentation();
		console.log('Mi nombre es ' + this.name);
		console.log('Mi power is ' + this.power);
	}
}

class Humanoide extends Character {
	name: string;
	velocity: number;
	hability: string;
	power: number;
	landa: number;
	static type: string = 'humanoide';

	constructor(mass: number, name: string, velocity: number, hability: string) {
		super(mass);

		this.name = name;
		this.velocity = velocity;
		this.hability = hability;
		this.landa = this.getHabilityPoints(hability);
		this.power = this.mass * this.velocity * this.landa;

		this.presentation();
	}

	private getHabilityPoints(hability: string): number {
		const habilities = {
			joker: 1,
			loving: 1,
			feminist: 4,
			seriesTVlover: 4,
		};
		// i know that this is not acceptable, but i dont find the solution in Nodejs
		// is simple only you have todo that: habilities[hability] but in tsc this solution throw and error
		if (hability == 'joker') return habilities.joker;
		if (hability == 'loving') return habilities.loving;
		if (hability == 'feminist') return habilities.feminist;
		if (hability == 'seriesTVlover') return habilities.seriesTVlover;
		return 0;
	}

	attack() {
		console.log('##################### ' + Humanoide.type + ' attack!!');
		console.log(this.name + ' attacks with ' + this.power);
	}
	presentation() {
		super.presentation();
		console.log('Mi nombre es ' + this.name);
		console.log('Mi power is ' + this.power);
	}
}

class Battle {
	private character1: Soldier;
	private character2: Humanoide;
	private winner: any;
	private losser: any;

	constructor(character1: Soldier, character2: Humanoide) {
		this.character1 = character1;
		this.character2 = character2;
		this.winner = null;
		this.losser = null;
	}
	private whoAttacks() {
		return Math.floor(Math.random() * 2 + 1);
	}
	public LetsGo() {
		while (this.character1.power > 0 && this.character2.power > 0) {
			let turno = this.whoAttacks();
			console.log(turno);
			if (turno == 1) {
				this.character1.attack();
				this.character2.power -= this.character1.power;
			} else if (turno == 2) {
				this.character2.attack();
				this.character1.power -= this.character2.power;
			} else {
				console.log('revisa tu codigo');
			}
			console.log(
				this.character1.name +
					' --> ' +
					this.character1.power +
					'|' +
					this.character2.power +
					' <-- ' +
					this.character2.name
			);
		}
		if (this.character1.power > this.character2.power) {
			this.winner = this.character1;
			this.losser = this.character2;
		} else {
			this.winner = this.character2;
			this.losser = this.character1;
		}
		console.log('-----------------------------------------------');
		console.log('and the winner is ...' + this.winner.name);
		console.log('with ' + this.winner.power + ' points of power');
		console.log('-----------------------------------------------');
		console.log('and the second is ...' + this.losser.name);
		console.log('with ' + this.losser.power + ' points of power');
		console.log('-----------------------------------------------');
	}
}

class Game {
	soldier: Soldier;
	humanoide: Humanoide;
	battle: Battle;

	constructor() {
		this.soldier = new Soldier(100, 'PapiChulo', 13, 'powerfull');
		this.humanoide = new Humanoide(80, 'Capuleto', 54, 'loving');

		this.battle = new Battle(this.soldier, this.humanoide);
		this.battle.LetsGo();
	}
}

var game = new Game();
