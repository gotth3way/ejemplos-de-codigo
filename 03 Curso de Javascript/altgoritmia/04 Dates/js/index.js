class algoritmia {
	static MESES = [
		'',
		'Enero',
		'Febrero',
		'Marzo',
		'Abril',
		'Mayo',
		'Junio',
		'Julio',
		'Agosto',
		'Septiembre',
		'Octubre',
		'Noviembre',
		'Diciembre',
	];

	constructor(number, title) {
		this.number = number;
		this.title = title;
		console.log('#####################################################');
		console.log(`${this.number}. ${this.title}`);
	}

	daysInMonth(m, y) {
		// m is 1 indexed: 1-12
		switch (m) {
			case 2:
				return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
			case 9:
			case 4:
			case 6:
			case 11:
				return 30;
			default:
				return 31;
		}
	}
	validarFechas(date) {
		var RegExPattern = /^\d{4}\/\d{1,2}\/\d{1,2}$/;
		if (RegExPattern.test(date) && date != '') {
			let arrayDate = date.split('/');
			let day = parseInt(arrayDate[2]); // 1 to 28, 30 o 31 it depends
			let month = parseInt(arrayDate[1]); // 1 to 12
			let year = parseInt(arrayDate[0]); // cualquiera que haya pasado el regExp
			console.log(`Day: ${day}, month: ${algoritmia.MESES[month]}, year: ${year}`);
			// comprobamos que el mes es valido un mes 13 o 0 o -12 no sería válido
			if (month < 1 || month > 12) {
				return false;
			}
			// validamos los días dependiendo del año, por aquello de los bisiestos y dependiendo del mes
			let daysInMonth = this.daysInMonth(month, year);
			console.info(`max days in this month ${algoritmia.MESES[month]}, for the year: ${year}: ${daysInMonth}`);
			if (day > daysInMonth) {
				return false;
			}
			return true;
		} else {
			return false;
		}
	}
	hownManyDaysUntilToday(date) {
		if (this.validarFechas(date)) {
			let secondsGivenDate = new Date(date).getTime() / 1000; // tiempo desde 1011970 hasta esa fecha en ms
			let secondsToday = Date.now() / 1000; // tiempo actual en ms desde 01011970/ 1000 -> s
			// console.log(secondsGivenDate, secondsToday);
			let secondsInOneDay = 60 * 60 * 24; // seg > min > horas
			let daysFromToday = Math.floor((secondsToday - secondsGivenDate) / secondsInOneDay);

			return daysFromToday;
		} else {
			console.warn('¡debes introducir una fecha válida! aaaa/mm/dd que exista, 2000/14/32 no es valida');
		}
		return false;
	}
	nuevoMetodoBase(date) {
		if (typeof date === 'Date') {
		} else {
			console.warn('¡debes introducir una fecha válida!');
		}
		return false;
	}
}

// ejercicio 1 longitud de strings
const exercice01 = new algoritmia(
	1,
	'Programa una función que dada una fecha calcule los días que faltan hasta la fecha actual'
);
console.log(exercice01.hownManyDaysUntilToday('2012/02/32')); // false
console.log(exercice01.hownManyDaysUntilToday('2012/22/32')); // false
console.log(exercice01.hownManyDaysUntilToday('1976/02/29')); // 16505
console.log(exercice01.hownManyDaysUntilToday('2012/02/29')); // 3356
console.log(exercice01.hownManyDaysUntilToday('2021/05/07')); // 1  today is 2021/05/08
