class algoritmia {
	constructor(number, title) {
		this.number = number;
		this.title = title;
		console.log('#####################################################');
		console.log(`${this.number}. ${this.title}`);
	}
	randomBetweenTwoNumbers(x = 0, y = 1, isInt = true) {
		if (typeof x === 'number' && typeof y === 'number') {
			if (isInt === true) {
				x = Math.round(x);
				y = Math.round(y);
				return Math.round(Math.random() * (y - x) + x);
			} else {
				return Math.random() * (y - x) + x;
			}
		} else {
			console.warn('debes introducir 2 números enteros');
		}
		return false;
	}
	sumAllBetweenTwoNumbers(x = 0, y = 1) {
		if (typeof x === 'number' && typeof y === 'number') {
			if (
				(Math.sign(x) === 0 || (Math.sign(x) === 1 && Math.sign(y) === 0) || Math.sign(y) === 1) &&
				Math.round(x) === x &&
				Math.round(y) === y
			) {
				let parejas = (y - (x - 1)) / 2;
				if (parejas % 2 == 0) {
					// console.log('Y');
					return (x + y) * parejas;
				} else {
					// console.log('N');
					parejas = Math.floor(parejas);
					let incremento = x + parejas;
					return (x + y) * parejas + incremento;
				}
				// suma de sucesion de enteros poisitvos el primero más el último * por la cantidad de parejas existentes
			} else {
				console.warn('debes introducir 2 números enteros y positivos');
			}
		} else {
			console.warn('debes introducir 2 números');
		}
		return false;
	}
	isCapicua(number = 0) {
		if (typeof number === 'number' && Math.round(number) === number) {
			let positiveNumber = Math.abs(number);
			let rebmun = positiveNumber.toString().split('').reverse().join('');
			if (rebmun == positiveNumber) {
				// becarefully here it you put === ... number  != string
				console.info(`yeah! ${number} is capicua`);
				return true;
			} else {
				console.info(`Nooo! ${number} is not a capicua`);
				return false;
			}
		} else {
			console.warn('debes introducir 1 número entero');
		}
		return false;
	}
	factorialRecursivo(n) {
		if (n == 0) {
			return 1;
		}
		return n * this.factorialRecursivo(n - 1);
	}
	factorial(number) {
		if (typeof number === 'number' && Math.round(number) === number && Math.abs(number) === number) {
			// hechas las validadciones usaremos la recursividad
			return this.factorialRecursivo(number);
		} else {
			console.warn('¡debes introducir 1 número entero positivo!');
		}
		return false;
	}
	primeTrueFalse(number) {
		let factor = number - 1;
		while (factor > 1) {
			if (number % factor === 0) {
				return false;
			}
			factor--;
		}
		return true;
	}

	isPrime(number) {
		if (typeof number === 'number' && Math.round(number) === number && Math.abs(number) === number) {
			if (number == 0 || number == 1) {
				console.warn(`El número ${number}, no es primo`);
				return false;
			} else {
				if (this.primeTrueFalse(number) === true) {
					console.info(`El número ${number}, es primo`);
				} else {
					console.info(`El número ${number}, no es primo`);
				}
			}
		} else {
			console.warn('¡debes introducir 1 número entero positivo!');
		}
		return false;
	}
	givemePrimes(number) {
		if (typeof number === 'number' && Math.round(number) === number && Math.abs(number) === number) {
			if (number == 0 || number == 1) {
				console.warn(`El número ${number}, no es válido, debe ser mayor a 1`);
				return false;
			} else {
				let arrayPrimes = [];
				while (number > 1) {
					if (this.primeTrueFalse(number) === true) {
						arrayPrimes.push(number);
					}
					number--;
				}
				return arrayPrimes.reverse();
			}
		} else {
			console.warn('¡debes introducir 1 número entero positivo!');
		}
		return false;
	}
	givemePrimesBetweenTwoNumbers(numberA = 0, numberB) {
		if (!numberB || !numberA) {
			console.error('Debes introducir dos números, el segundo mayor que el primero ');
			return false;
		}
		if (
			typeof numberA === 'number' &&
			Math.round(numberA) === numberA &&
			Math.abs(numberA) === numberA &&
			typeof numberB === 'number' &&
			Math.round(numberB) === numberB &&
			Math.abs(numberB) === numberB
		) {
			if (numberA == 0) {
				console.warn(`El número ${numberA}, no es válido, debe ser mayor que 1`);
				return false;
			}
			if (numberB <= numberA) {
				console.warn(`El número ${numberB}, no es válido, debe ser mayor que ${numberA}`);
				return false;
			} else {
				let arrayPrimes = [];
				while (numberB >= numberA) {
					if (this.primeTrueFalse(numberB) === true) {
						arrayPrimes.push(numberB);
					}
					numberB--;
				}
				return arrayPrimes.reverse();
			}
		} else {
			console.warn('¡debes introducir números enteros positivos!');
		}
		return false;
	}
	convertFahrenheitToCelsius(number) {
		if (typeof number === 'number') {
			console.info(`Fahrenheit: ${number} convert to Celsius:`);
			return ((number - 32) / 1.8).toFixed(2);
		} else {
			console.warn('¡debes introducir un número!');
		}
		return false;
	}
	convertCelsiusToFahrenheit(number) {
		if (typeof number === 'number') {
			console.info(`Fahrenheit: ${number} convert to Celsius:`);
			return (number * 1.8 + 32).toFixed(2);
		} else {
			console.warn('¡debes introducir un número!');
		}
		return false;
	}
}

// ejercicio 1 longitud de strings
const exercice01 = new algoritmia(
	1,
	'Programa una función que devuelva un número aleatorio entre "x" y "y", dos números enteros'
);
console.log(exercice01.randomBetweenTwoNumbers(1, 100, false)); // 95.03403040324040
console.log(exercice01.randomBetweenTwoNumbers(1, 100, true)); // 35
console.log(exercice01.randomBetweenTwoNumbers(100, 1)); // 40
console.log(exercice01.randomBetweenTwoNumbers('pollo')); // false
console.log(exercice01.randomBetweenTwoNumbers('pollo', 3.4)); // false
console.log(exercice01.randomBetweenTwoNumbers(5, 5)); // 5

// ejercicio 2 longitud de strings
const exercice02 = new algoritmia(
	2,
	'Programa una función que sume todos los enteros positivos entre "x" y "y". sin recorrer la sucesion. Utilizando alguna artimaña.'
);
console.log(exercice02.sumAllBetweenTwoNumbers(1, 100)); // 5050
console.log(exercice02.sumAllBetweenTwoNumbers(2, 10)); // 54
console.log(exercice02.sumAllBetweenTwoNumbers(100, 1000));

// ejercicio 3 capicua
const exercice03 = new algoritmia(
	3,
	'Programa una función que sume todos los enteros positivos entre "x" y "y". sin recorrer la sucesion. Utilizando alguna artimaña.'
);
console.log(exercice03.isCapicua(101)); // true
console.log(exercice03.isCapicua(606)); // true
console.log(exercice03.isCapicua(103457475475471)); // false
console.log(exercice03.isCapicua(-606)); // true

// ejercicio 4 capicua
const exercice04 = new algoritmia(4, 'Programa una función que calcule el factorial de un número entero positivo');
console.log(exercice04.factorial(-1)); // false
console.log(exercice04.factorial(0.5)); // false
console.log(exercice04.factorial('perro')); // false
console.log(exercice04.factorial(13)); // 6227020800
console.log(exercice04.factorial(0)); // 1
console.log(exercice04.factorial(200)); // Infinity

// ejercicio 5 primo
const exercice05 = new algoritmia(5, 'Programa una función que nos muestre si un número es primo');
console.log(exercice05.isPrime(-1)); // false
console.log(exercice05.isPrime(0.5)); // false
console.log(exercice05.isPrime('perro')); // false
console.log(exercice05.isPrime(13)); // true
console.log(exercice05.isPrime(2)); // true
console.log(exercice05.isPrime(0)); // false
console.log(exercice05.isPrime(200)); // false

// ejercicio 6 primo
const exercice06 = new algoritmia(
	6,
	'Programa una función que nos devuelva un array con todos los números primos desde el 2 hasta el número dado'
);
console.log(exercice06.givemePrimes(-1)); // false
console.log(exercice06.givemePrimes(0.5)); // false
console.log(exercice06.givemePrimes('perro')); // false
console.log(exercice06.givemePrimes(13)); // [2, 3, 5, 7, 11, 13]
console.log(exercice06.givemePrimes(2)); // [2]
console.log(exercice06.givemePrimes(0)); // false
console.log(exercice06.givemePrimes(200)); // [2, 3, 5, 7, 11, 13, 17, 19, 23, ...]
// console.log(exercice06.givemePrimes(200000)); // [2, 3, 5, 7, 11, 13, 17, 19, 23, ...] stack overflow

// ejercicio 7 primo
const exercice07 = new algoritmia(
	7,
	'Programa una función que nos devuelva un array con todos los números primos desde el 2 hasta el número dado'
);
console.log(exercice07.givemePrimesBetweenTwoNumbers(-1)); // false
console.log(exercice07.givemePrimesBetweenTwoNumbers(0.5)); // false
console.log(exercice07.givemePrimesBetweenTwoNumbers('perro')); // false
console.log(exercice07.givemePrimesBetweenTwoNumbers(13)); // false
console.log(exercice07.givemePrimesBetweenTwoNumbers(13, 27)); // [13, 17, 19, 23]
console.log(exercice07.givemePrimesBetweenTwoNumbers(27, 13)); // false
console.log(exercice07.givemePrimesBetweenTwoNumbers(27, 27)); // false
console.log(exercice07.givemePrimesBetweenTwoNumbers(28000, 28100)); // [28001, 28019, 28027, 28031, 28051, 28057, 28069, 28081, 28087, 28097, 28099]

// ejercicio 8 primo
const exercice08 = new algoritmia(8, 'Programa una función que nos convierta de celsius a Fahrenheit y viceversa.');
console.log(exercice08.convertFahrenheitToCelsius(-1)); // -18.33
console.log(exercice08.convertFahrenheitToCelsius(10)); // -12.22
console.log(exercice08.convertFahrenheitToCelsius(20)); // -6.67
console.log(exercice08.convertFahrenheitToCelsius(0)); // -17.78
console.log(exercice08.convertCelsiusToFahrenheit(-234)); // -309.20
console.log(exercice08.convertCelsiusToFahrenheit(32)); // 89.60
console.log(exercice08.convertCelsiusToFahrenheit(112)); // 233.60
console.log(exercice08.convertCelsiusToFahrenheit(0)); // 32.00
console.log(exercice08.convertCelsiusToFahrenheit({ name: 'Jhon', surname: 'Moon' })); // 32.00
