class algoritmia {
	constructor(number, title) {
		this.number = number;
		this.title = title;
		console.log('#####################################################');
		console.log(`${this.number}. ${this.title}`);
	}
	static isPar(number) {
		return number % 2 === 0;
	}
	static bigger(numberA, numberB) {
		if (numberA > numberB) {
			return numberA;
		} else {
			return numberB;
		}
	}
	static arrayEquals(array1, array2) {
		if (array1.length != array2.length) return false;

		for (var i = 0; i < array2.length; i++) {
			if (array1[i] instanceof Array && array2[i] instanceof Array) {
				if (!array1[i].equals(array2[i])) return false;
			} else if (array1[i] != array2[i]) {
				return false;
			}
		}
		return true;
	}
	static test() {
		let countErrors = 0,
			testPassed = false;

		// isPar
		console.info('##### testing ##################################');
		algoritmia.isPar(7) === false ? (testPassed = true) : countErrors++;
		console.info(` isPar(7) errors test1: ${countErrors}`);
		algoritmia.isPar(-8) === true ? (testPassed = true) : countErrors++;
		console.info(` isPar(8) errors test2: ${countErrors}`);
		if (countErrors > 0) {
			console.error(`tests failure`);
		} else {
			console.info(`tests Passed`);
		}
		// bigger
		countErrors = 0;
		testPassed = false;
		console.info('##### testing ##################################');
		algoritmia.bigger(8, 18) === 18 ? (testPassed = true) : countErrors++;
		console.info(` bigger(8, 18) errors test1: ${countErrors}`);
		algoritmia.bigger(-8, -77) === -8 ? (testPassed = true) : countErrors++;
		console.info(` bigger(-8, -77) errors test2: ${countErrors}`);
		if (countErrors > 0) {
			console.error(`tests failure`);
		} else {
			console.info(`tests Passed`);
		}
		// validateArrayNumbers
		countErrors = 0;
		testPassed = false;
		let test1 = new algoritmia(1, '##### testing ##################################');
		test1.validateArrayNumbers([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) === true ? (testPassed = true) : countErrors++;
		console.info(`validateArrayNumbers([1,2,3,4,5,6,7,8,9,0])  errors test1: ${countErrors}`);
		test1.validateArrayNumbers([1, 2, 3, 4, 5, 6, 7, 8, 'arbol', 0]) === false ? (testPassed = true) : countErrors++;
		console.info(`validateArrayNumbers([1,2,3,4,5,6,7,8,'arbol',0])  errors test1: ${countErrors}`);
		if (countErrors > 0) {
			console.error(`tests failure`);
		} else {
			console.info(`tests Passed`);
		}
		// arrayPows
		countErrors = 0;
		testPassed = false;
		let test2 = new algoritmia(2, '##### testing ##################################');
		algoritmia.arrayEquals(test2.arrayPows([1, 2, 3, 4]), [1, 4, 9, 16]) ? (testPassed = true) : countErrors++;
		console.info(`arrayPows([1, 2, 3, 4])  errors test1: ${countErrors}`);
		test2.arrayPows([8, 'arbol', 0]) === false ? (testPassed = true) : countErrors++;
		console.info(`arrayPows([8,'arbol',0])  errors test1: ${countErrors}`);
		if (countErrors > 0) {
			console.error(`tests failure`);
		} else {
			console.info(`tests Passed`);
		}
		// evensAndOdds
		countErrors = 0;
		testPassed = false;
		let test3 = new algoritmia(3, '##### testing ##################################');
		(test3.arraysEvenAndOdd([1, 2, 3, 4]), { evens: [2, 4], odds: [1, 3] }) ? (testPassed = true) : countErrors++;
		console.info(`arrayPows([1, 2, 3, 4])  errors test1: ${countErrors}`);
		test2.arrayPows([8, 'arbol', 0]) === false ? (testPassed = true) : countErrors++;
		console.info(`arrayPows([8,'arbol',0])  errors test1: ${countErrors}`);
		if (countErrors > 0) {
			console.error(`tests failure`);
		} else {
			console.info(`tests Passed`);
		}
		// biggerAndSmaller
		countErrors = 0;
		testPassed = false;
		let test4 = new algoritmia(4, '##### testing ##################################');
		(test4.arrayBiggerAndSmaller([1, 2, 3, 4]), { bigger: 4, smaller: 1 }) ? (testPassed = true) : countErrors++;
		console.info(`arrayPows([1, 2, 3, 4])  errors test1: ${countErrors}`);
		test2.arrayPows([8, 'arbol', 0]) === false ? (testPassed = true) : countErrors++;
		console.info(`arrayPows([8,'arbol',0])  errors test1: ${countErrors}`);
		if (countErrors > 0) {
			console.error(`tests failure`);
		} else {
			console.info(`tests Passed`);
		}
		// arraysort
		countErrors = 0;
		testPassed = false;
		let test5 = new algoritmia(5, '##### testing ##################################');
		algoritmia.arrayEquals(test5.arraySortingHiggerOrLower([1, 2, 3, 4]), [1, 2, 3, 4])
			? (testPassed = true)
			: countErrors++;
		console.info(`arrayPows([1, 2, 3, 4])  errors test1: ${countErrors}`);
		test2.arrayPows([8, 'arbol', 0]) === false ? (testPassed = true) : countErrors++;
		console.info(`arrayPows([8,'arbol',0])  errors test1: ${countErrors}`);
		if (countErrors > 0) {
			console.error(`tests failure`);
		} else {
			console.info(`tests Passed`);
		}
	}
	validateArrayNumbers(posibleArray) {
		try {
			let errors = 0;
			posibleArray.forEach((element) => {
				if (typeof element !== 'number') {
					errors++;
				}
			});
			if (errors > 0) {
				return false;
			} else {
				return true;
			}
		} catch {
			return false;
		}
	}
	arrayPows(numbers) {
		if (this.validateArrayNumbers(numbers)) {
			let newArrayNumbers = [];
			numbers.forEach((element) => {
				newArrayNumbers.push(Math.pow(element, 2));
			});
			// console.log(newArrayNumbers);
			return newArrayNumbers;
		} else {
			console.warn(`${numbers} is not a valid unidimensional array of numbers`);
			return false;
		}
	}
	arraysEvenAndOdd(numbers) {
		if (this.validateArrayNumbers(numbers)) {
			let arrayNumbersEven = [];
			let arrayNumbersOdd = [];
			numbers.forEach((element) => {
				if (element % 2 === 0) {
					arrayNumbersEven.push(element);
				} else {
					arrayNumbersOdd.push(element);
				}
			});
			// console.log(newArrayNumbers);
			return { evens: arrayNumbersEven, odds: arrayNumbersOdd };
		} else {
			console.warn(`${numbers} is not a valid unidimensional array of numbers`);
			return false;
		}
	}
	arrayBiggerAndSmaller(numbers) {
		if (this.validateArrayNumbers(numbers)) {
			let bigger;
			let smaller;
			let sorted = numbers.sort((a, b) => a - b); // oredenar numericamente en vez de alfabeticamente
			console.log(sorted);
			bigger = sorted[numbers.length - 1];
			smaller = sorted[0];
			return { smaller: smaller, bigger: bigger };
		} else {
			console.warn(`${numbers} is not a valid unidimensional array of numbers`);
			return false;
		}
	}
	arraySortingHiggerOrLower(numbers, flag = 'm') {
		if (this.validateArrayNumbers(numbers)) {
			let sorted = numbers.sort((a, b) => a - b); // oredenar numericamente en vez de alfabeticamente
			if (flag == 'm') {
				// console.log(sorted);
				return sorted;
			} else {
				// console.log(sorted.reverse());
				return sorted.reverse();
			}
		} else {
			console.warn(`${numbers} is not a valid unidimensional array of numbers`);
			return false;
		}
	}
}

algoritmia.test();

// ejercicio 1
const exercice01 = new algoritmia(
	1,
	'Programa una función que dado un array numérico devuelva otro con los mismos números elevados al cuadrado'
);

console.log(exercice01.arrayPows(['arbol', 2, 3, 4])); // false
console.log(exercice01.arrayPows(['1', 2, 3, 4])); // false
console.log(exercice01.arrayPows([1, 2, 3, 4, [1, 2]])); // false
console.log(exercice01.arrayPows([1, 2, 3, 4])); // [1, 4, 9, 16]
console.log(exercice01.arrayPows([1, 2, 3, 4, 27, 45678])); // [1, 4, 9, 16, 729, 2086479684]

// ejercicio 2
const exercice02 = new algoritmia(
	2,
	'Programa una función que dado un array numérico extraiga los pares en un array y los impares en otro array dentro de un solo objeto'
);

console.log(exercice02.arraysEvenAndOdd(['arbol', 2, 3, 4])); // false
console.log(exercice02.arraysEvenAndOdd(['1', 2, 3, 4])); // false
console.log(exercice02.arraysEvenAndOdd([1, 2, 3, 4, [1, 2]])); // false
console.log(exercice02.arraysEvenAndOdd([1, 2, 3, 4])); // {evens: [2, 4], odds: [1, 3]}
console.log(exercice02.arraysEvenAndOdd([1, 2, 3, 4, 27, 45678])); // {evens: [2, 4 45678], odds: [1, 3, 27]}

// ejercicio 3
const exercice03 = new algoritmia(
	3,
	'Programa una función que dado un array de números devuelva el más alto y el más bajo'
);
console.log(exercice02.arraysEvenAndOdd(['arbol', 2, 3, 4])); // false
console.log(exercice03.arrayBiggerAndSmaller([27, 9, -10, 1198, 320, 1.2, 0.001]));

// ejercicio 4
const exercice04 = new algoritmia(
	4,
	'Programa una función que dado un array de números lo devuelva ordenado de mayor a menor o de menor a mayor'
);
console.log(exercice04.arraySortingHiggerOrLower([-111, 222, 33, 4.4, 5.9, 5.6, -7, 'albariño'], 'M')); // false
console.log(exercice04.arraySortingHiggerOrLower([1, 2, 3, 4, 5, 6, 7, 8])); // [1,2,3,4,5,6,7,8]
console.log(exercice04.arraySortingHiggerOrLower([1, 2, 3, 4, 5, 6, 7, 8], 'M')); // [1,2,3,4,5,6,7,8]
console.log(exercice04.arraySortingHiggerOrLower([-111, 222, 33, 4.4, 5.9, 5.6, -7, 88888], 'm')); // [-111, -7, 4.4, 5.6, 5.9, 33, 222, 88888]
console.log(exercice04.arraySortingHiggerOrLower([-111, 222, 33, 4.4, 5.9, 5.6, -7, 88888], 'M')); // [88888, 222, 33, 5.9, 5.6, 4.4, -7, -111]

// // ejercicio 5
// const exercice05 = new algoritmia(5, 'Programa una función que dado un array de números extraiga los repetidos');
// metodo 1
// var a = ['a', 1, 'a', 2, '1'];
// for (var i = a.length - 1; i >= 0; i--) {
// 	if (a.indexOf(a[i]) !== i) a.splice(i, 1);
// }
// metodo 2 moderno
// var a = ['a', 1, 'a', 2, '1'];
// var unique = a.filter(onlyUnique);

// // ejercicio 6 promedio
// const exercice06 = new algoritmia(6, 'Programa una función que dado un array de números devuelva el promedio');
// var a = [2, 1, 3, 4, 6]; let calc = 0;
// for (var i = a.length - 1; i >= 0; i--) {
// 	calc += a[i];
// }
// return calc / a.length
