class algoritmia {
	constructor(number, title) {
		this.number = number;
		this.title = title;
		console.log('#####################################################');
		console.log(`${this.number}. ${this.title}`);
	}
	longString(aString = '') {
		console.log(aString);
		try {
			return aString.length;
		} catch (error) {
			console.log(`Ciertos tipos, como ${typeof aString}, no pueden ser utilizados con esta función`);
			return null;
		}
	}
	sliceNumberCharacters(aString = '', numberCharacters = 0) {
		console.log('------------------------------------------------------');
		console.log(aString);
		console.log('result: ');
		try {
			return aString.slice(0, numberCharacters);
		} catch (error) {
			console.log(`Ciertos tipos, como ${typeof aString}, no pueden ser utilizados con esta función`);
			return null;
		}
	}
	splitWithCharacterToArray(aString = '', character = '') {
		console.log('------------------------------------------------------');
		console.log(aString);
		console.log('result: ');
		if (typeof aString === 'string' && typeof character === 'string') {
			return aString.split(character);
		}
		return false;
	}
	repeatText(aString = '', times = 1) {
		console.log('------------------------------------------------------');
		console.log(aString);
		console.log('result: ');

		let resultado = '';
		if (typeof aString === 'string' && typeof times === 'number') {
			if (Math.sign(times) === 1 && Math.round(times) === times) {
				// es un numero positivo y no decimal
				for (let i = 0; i < times; i++) {
					resultado = aString + resultado;
				}
				return resultado;
			} else {
				console.warn('¡¡introduce valores correctos!!');
			}
		} else {
			console.warn('¡¡debes introudcir un string válido');
		}
		return false;
	}
	revertText(aString = '') {
		console.log('------------------------------------------------------');
		console.log(aString);
		console.log('result: ');

		if (typeof aString === 'string') {
			// también podría usarse el método Array.reverse() y join()
			// cadena.split("").reverse().join("")
			let cadenaInvertida = '';
			let n = aString.length;
			for (let i = n; i >= 0; i--) {
				cadenaInvertida = cadenaInvertida + aString.charAt(i);
			}
			return cadenaInvertida;
		} else {
			console.warn('¡¡debes introudcir un string válido');
		}
		return false;
	}
	formatTextWhitoutSpaces(aString = '') {
		console.log('------------------------------------------------------');
		console.log(aString);
		console.log('result: ');

		if (typeof aString === 'string') {
			let cadenaNueva = '';
			let arrayText = aString.split(' ');
			let n = arrayText.length;
			for (let i = 0; i < n; i++) {
				cadenaNueva = cadenaNueva + arrayText[i];
			}
			return cadenaNueva;
		} else {
			console.warn('¡¡debes introudcir un string válido');
		}
		return false;
	}
	allInLowercase(aString = '') {
		console.log('------------------------------------------------------');
		console.log(aString);
		console.log('result: ');
		if (typeof aString === 'string') {
			return aString.toLowerCase();
		} else {
			console.warn('¡¡debes introudcir un string válido');
		}
		return false;
	}
	cleanSignes(aString = '') {
		// todo a mayúscula
		aString = aString.toLowerCase();
		console.log(`to lower case: ${aString}`);
		// quitar espacios en blanco
		aString = aString.replace(/ /g, ''); // reemplaza todos los ' ' por ''
		console.log(`spaces out: ${aString}`);
		// quitar acentos
		aString = aString
			.normalize('NFD')
			.replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi, '$1')
			.normalize(); // NFC to NFD normaliza debido a utf8
		console.log(`accents out : ${aString}`);
		// quitar todos los signos de puntuacion
		aString = aString.replace(/[.,;:\/#!$%\^&\*;:{}=\-_`~()]/g, '');
		console.log(`clean all signes: ${aString}`);

		return aString;
	}
	isPalindrom(aString = '') {
		console.log('------------------------------------------------------');
		console.log(aString);
		console.log('result: ');
		if (typeof aString === 'string') {
			let cadenaLimpia = this.cleanSignes(aString);
			let cadenaLimpiaInvertida = '';
			//comprobar si es palíndromo, invirtiendo aString guardandola en cadenaLimpiaInvertida y comprobar si son iguales
			let n = cadenaLimpia.length;
			for (let i = n; i >= 0; i--) {
				cadenaLimpiaInvertida = cadenaLimpiaInvertida + cadenaLimpia.charAt(i);
			}
			if (cadenaLimpia == cadenaLimpiaInvertida) return true;
		} else {
			console.warn('¡¡debes introudcir un string válido');
		}
		return false;
	}
	howTimesInString(text = '', word = ' ', ignoreCase = false) {
		console.log('------------------------------------------------------');
		console.log(`texto: ${text} \nword: ${word}`);
		console.log('result: ');
		if (typeof text === 'string' && typeof word === 'string') {
			if (ignoreCase) {
				text = text.toLowerCase();
				word = word.toLowerCase();
			}
			let i = 0;
			let contador = 0;
			while (i !== -1) {
				i = text.indexOf(word, i);
				if (i !== -1) {
					i++;
					contador++;
				}
			}
			return contador;
		} else {
			console.warn('debes ingresar dos strings');
		}
		return false;
	}
	replacePatternInString(text = '', pattern = ' ', ignoreCase = false) {
		console.log('------------------------------------------------------');
		console.log(`texto: ${text} \pattern: ${pattern}`);
		console.log('result: ');
		if (typeof text === 'string' && typeof pattern === 'string') {
			if (ignoreCase) {
				text = text.toLowerCase();
				pattern = pattern.toLowerCase();
				var regExp = new RegExp(pattern, 'ig');
			} else {
				var regExp = new RegExp(pattern, 'g');
			}
			return text.replace(regExp, '');
		} else {
			console.warn('¡¡debes introudcir un string válido');
		}
		return false;
	}
}

const lorem =
	'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took';
const person = {
	name: 'John',
	surname: 'Moon',
	age: 45,
};
const anArrayNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
// ejercicio 1 longitud de strings
const exercice01 = new algoritmia(
	1,
	'Encontrar la longitud de un string, array si es que lo es, y si no devolver un mensaje de error'
);
console.log(exercice01.longString('texto debe devolver 22'));
console.log(exercice01.longString(anArrayNumbers));
console.log(exercice01.longString(person));

// ejercicio 2: slice strings

const exercice02 = new algoritmia(
	2,
	'Programa una función que te devuelva el texto o el array recortado según el número de caracteres indicado, o un mensaje de error. En caso que sea mayor  lo devolverá entero'
);
console.log(exercice02.sliceNumberCharacters(lorem, 10)); // "Lorem Ipsu"
console.log(exercice02.sliceNumberCharacters(anArrayNumbers, 5)); // [1,2,3,4,5]
console.log(exercice02.sliceNumberCharacters(anArrayNumbers, 15)); // [1,2,3,4,5,6,7,8,9,0]
console.log(exercice02.sliceNumberCharacters(person, 22)); // error
console.log(exercice02.sliceNumberCharacters(null, 22)); // error

// ejercicio 3: split strings

const exercice03 = new algoritmia(
	3,
	'Programa una función que dado un string te devuelva un array de textos separados por un caracter dado, solo para strings'
);
console.log(exercice03.splitWithCharacterToArray(lorem, ' ')); // ["Lorem", "Ipsum", "is", "simply", "dummy", "text", "of", "the", "printing", "and", "typesetting", "industry.", "Lorem", "Ipsum", "has", "been", "the", "industrys", "standard", "dummy", "text", "ever", "since", "the", "1500s,", "when", "an", "unknown", "printer", "took"]
console.log(exercice03.splitWithCharacterToArray(lorem, '')); // ["L", "o", "r", "e", "m", " ", "I", "p", "s", "u", "m", " ", "i", "s", " ", "s", "i", "m", "p", "l", "y", " ", "d", "u", "m", "m", "y", " ", "t", "e", "x", "t", " ", "o", "f", " ", "t", "h", "e", " ", "p", "r", "i", "n" ...
console.log(exercice03.splitWithCharacterToArray(anArrayNumbers, 1)); // false
console.log(exercice03.splitWithCharacterToArray(anArrayNumbers, ' ')); // false
console.log(exercice03.splitWithCharacterToArray(person, ' ')); // false
console.log(exercice03.splitWithCharacterToArray(null, ' ')); // false

// ejercicio 4: repeat strings

const exercice04 = new algoritmia(
	4,
	'Programa una función que dado un string (solo strings) y un "numero", te devuelva ese string repetido n veces= "numero"'
);
console.log(exercice04.repeatText(lorem, 3)); // ["Lorem", "Ipsum", "is", "simply", "dummy", "text", "of", "the", "printing", "and", "typesetting", "industry.", "Lorem", "Ipsum", "has", "been", "the", "industrys", "standard", "dummy", "text", "ever", "since", "the", "1500s,", "when", "an", "unknown", "printer", "took"]
console.log(exercice04.repeatText(lorem, 0)); // ["L", "o", "r", "e", "m", " ", "I", "p", "s", "u", "m", " ", "i", "s", " ", "s", "i", "m", "p", "l", "y", " ", "d", "u", "m", "m", "y", " ", "t", "e", "x", "t", " ", "o", "f", " ", "t", "h", "e", " ", "p", "r", "i", "n" ...
console.log(exercice04.repeatText(anArrayNumbers, 2)); // false
console.log(exercice04.repeatText(anArrayNumbers, ' ')); // false
console.log(exercice04.repeatText(person, 3)); // false
console.log(exercice04.repeatText(null, 1000)); // false

// ejercicio 5: repeat strings

const exercice05 = new algoritmia(5, 'Programa una función que dado un string la invierta');
console.log(exercice05.revertText(lorem)); //
console.log(exercice05.revertText('pollo')); // ollop
console.log(exercice05.revertText(anArrayNumbers, 2)); // false
console.log(exercice05.revertText(anArrayNumbers, ' ')); // false
console.log(exercice05.revertText(person, 3)); // false
console.log(exercice05.revertText(null, 1000)); // false

// ejercicio 6: repeat strings

const exercice06 = new algoritmia(6, 'Programa una función que dado un string le quite todos los espacios en blanco');
console.log(exercice06.formatTextWhitoutSpaces(lorem)); //
console.log(exercice06.formatTextWhitoutSpaces('pollo con sobrasada')); // "pollloconsobrasada"
console.log(exercice06.formatTextWhitoutSpaces(anArrayNumbers)); // false
console.log(exercice06.formatTextWhitoutSpaces(anArrayNumbers)); // false
console.log(exercice06.formatTextWhitoutSpaces(person)); // false
console.log(exercice06.formatTextWhitoutSpaces(null)); // false

// ejercicio 7: repeat strings

const exercice07 = new algoritmia(7, 'Programa una función que dado un string devuelva este en minúsculas');
console.log(exercice07.allInLowercase(lorem)); //
console.log(exercice07.allInLowercase('Pollo Con SobrAsada')); // "polllo con sobrasada"
console.log(exercice07.allInLowercase(anArrayNumbers)); // false
console.log(exercice07.allInLowercase(anArrayNumbers)); // false
console.log(exercice07.allInLowercase(person)); // false
console.log(exercice07.allInLowercase(null)); // false

// ejercicio 8: repeat strings

const exercice08 = new algoritmia(8, 'Programa una función que dado un string compruebe si es un palíndromos');
console.log(exercice08.isPalindrom(lorem)); // false
console.log(exercice08.isPalindrom('Dábale arroz a la zorra, el abad')); // true
console.log(exercice08.isPalindrom('Isaac no ronca así')); // true
console.log(exercice08.isPalindrom('Sé verlas al revés.')); // true
console.log(exercice08.isPalindrom('Amó la paloma, Anita lava la tina, Yo hago yoga hoy')); // false
// Sé verlas al revés, Amó la paloma, Anita lava la tina, Yo hago yoga hoy

// ejercicio 9: cuantas veces se repite una cadena dentro de otra
const exercice09 = new algoritmia(
	9,
	'Programa una función que dado un string y una cadena, ver cuantas veces se repite el carácter en la cadena'
);
const travalenguas = 'Pepe perez peluquero peinaba pelucas por pocas pesetas y ponía puas en las peinetas';
console.log(exercice09.howTimesInString(lorem, 'lorem', true)); // 2
console.log(exercice09.howTimesInString(travalenguas, 'p', true)); // 12

// ejercicio 10: elimina un patrín dentro de un texto
const exercice10 = new algoritmia(10, 'Programa una función que dado un texto y un patron, sacar el patrón del texto');
console.log(exercice10.replacePatternInString('xyz1, xyz2, xyz3, xyz4', 'xyz', true)); // 1, 2, 3, 4,
console.log(exercice10.replacePatternInString(lorem, 'lorem', true)); //
console.log(exercice10.replacePatternInString(travalenguas, 'p', true)); // 12

// ejercicio 11: cuente las vocales y las consonantes de un texto
// const exercice11 = new algoritmia(11, 'Programa una función que dado un texto cuente las vocales y las consonantes del mismo');

// ejercicio 12: validar un email
// const exercice11 = new algoritmia(12, 'Programa una función que dado un email lo valide');
