module.exports = {
	name: 'ApiRoutes',
	register: async (server, options) => {
		server.route([
			{
				method: 'GET',
				path: '/',
				handler: async (req, res) => {
					return 'Hola Noders from Hapi!';
				},
			},
			{
				method: 'GET',
				path: '/users/{name?}', // optativo por eso lleva el ? al final
				handler: async (req, res) => {
					const name = req.params.name ? req.params.name : 'invitado'; // const name = req.params.name || 'invitado'
					return `Hola ${name}!`;
				},
			},
			{
				// OBSERVATIONS: para probar esta ruta debemos utilizar un cliente http como postman en peticion
				// a la url http:localhost:3000/users y format ode data urlencoded en el body poner name: "algo"
				// surname: "algomas"
				method: 'POST',
				path: '/users',
				handler: async (req, res) => {
					const newUser = {
						name: req.payload.name,
						surname: req.payload.surname,
					};
					return res
						.response({
							data: newUser,
						})
						.type('application/json');
				},
			},
			{
				method: 'PUT',
				path: '/users/{id}',
				handler: async (req, res) => {
					const newUser = {
						name: req.payload.name,
						surname: req.payload.surname,
					};
					return res
						.response({
							data: newUser,
							message: `Usuario ID: ${req.params.id} modificado exitósamente!`,
						})
						.type('application/json');
				},
			},
			{
				method: 'DELETE',
				path: '/users/{id}',
				handler: async (req, res) => {
					return res
						.response({
							message: `Usuario ID: ${req.params.id} eliminado exitósamente!`,
						})
						.type('application/json');
				},
			},
		]);
	},
};
