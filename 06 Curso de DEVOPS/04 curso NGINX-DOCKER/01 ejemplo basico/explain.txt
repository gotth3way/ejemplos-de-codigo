En docker-compose.yml tenemos la configuracion de tres contenedores:
1. app
2. nginx
3. mongo

los dos primeros tiene su Dockerfile y .dockerignore, el tercero es creado a partir de una imagen remota
y arranca ahí, sin necesidad de Dockerfile, con su configuracion por defecto.

para que funciones esto en remoto, no necesitas ni nginx, ni mongo, en la máquina.

Los contenedores instalan las imágenes dentro de esos contenedores y están aislados del SO de Linux.

Eso si, por seguridad si debes instalar ssh, para acceso al servidor, ufw y configurarlos.

Esta configuración tiene un nginx.conf muy simple. para extender sus habilidades, deberías crear algo como 
esto:
--------default.conf----------
server {
  listen 80;
  server_name example.com www.example.com;

  #Configura el directorio raíz servido públicamente
  #Configura el archivo de índice para ser servido
  root /var/www/example.com;
      index index.html index.htm;

  #Estas líneas crean un bypass para ciertos nombres de ruta
  #www.example.com/index.js ahora se enruta al puerto 3000. Para que funcione con nodejs
  #instead of port 80
  location /index.js {
      proxy_pass http://localhost:3000;
      proxy_set_header Host $host;
  }
}
---------default.conf----------

pero así solo funciona con un dominio y en la configuración por defecto de nginx.
Así que nosotros debemos ver como están configurado los dominios y subdominios y hacérselo saber 
al docker-compose.yml a través de su Dockerfile, explicándole donde debe copiar ese archivo:

/etc/nginx/sites-available/captionsconnection.com
/etc/nginx/sites-available/landingpage.captionsconnection.com

y posteriormente hacerle saber mediante un comando CMD, que haga el link de sites enabled





