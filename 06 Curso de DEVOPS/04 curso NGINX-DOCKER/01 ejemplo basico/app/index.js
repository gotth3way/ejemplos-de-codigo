const express = require('express');
const app = express();
var PORT;
if (process.env.NODE_ENV === 'production') {
	PORT = 6666;
} else {
	PORT = 5000;
}
app.get('/', (req, res) => {
	res.send('Hello World from node serve.js!');
});
app.listen(PORT, () => console.log('Server is up and running AT PORT: ' + PORT));
