#!/usr/bin/env sh
# $0 is a script name, 
# $1, $2, $3 etc are passed arguments  # $1 is our command 
CMD=$1
  
case "$CMD" in  
    "dev" ) 
		npm install 
		export NODE_ENV=development
		export PORT=3333
		exec npm run nodeDev 
		;;
		
	"prod" ) 
	# we can modify files here, using ENV variables passed in 
	# "docker create" command. It can't be done during build process. 
    # WARNING: I DONT NOW WHAT´S THAT:  "db: $DATABASE_ADDRESS" >> /app/config.yml ?????
	echo  "db: $DATABASE_ADDRESS" >> /var/www/captionsconnection.com/config.yml 
    ## WARNING: SEE build process pass variables de db
	export NODE_ENV=production 
	export PORT=6666 
	exec npm run nodeProd 
	;;

	* ) 
	 # Run custom command. Thanks to this line we can still use 
	 # "docker run our_image /bin/bash" and it will work  
	 exec $CMD ${@:2} 
	 ;; 
esac