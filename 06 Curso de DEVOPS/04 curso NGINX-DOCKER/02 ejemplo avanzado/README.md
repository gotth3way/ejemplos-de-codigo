### DOCKER OPTIMIZATION

## EXPLAINING

En docker-compose.yml tenemos la configuracion de tres contenedores:

1. app
2. nginx
3. mongo

los dos primeros tiene su Dockerfile y .dockerignore, el tercero es creado a partir de una imagen remota
y arranca ahí, sin necesidad de Dockerfile, con su configuracion por defecto.

para que funciones esto en remoto, no necesitas ni nginx, ni mongo, en la máquina, pero si en el contenedor, a no ser que el servicio de mongo lo pongas con mlab, en cuyo caso, el dockerfile para mongo, no se necesita ni la configuración para docker-compose sobre mongo.

Los contenedores instalan las imágenes dentro de esos contenedores y están aislados del SO de Linux.

Eso si, por seguridad si debes instalar ssh, para acceso al servidor, ufw y configurarlos.
debes asegurarte que hayas instalado los siguientes comandos o aplicaciones:

git, pm2 o nodemon, ssh, ufw, docker, docker-compose

haber creado el usuario adecuado con privilegios de root y meter al grupo del usuario a docker

Esta configuración tiene un nginx.conf muy simple. para extender sus habilidades, deberías crear algo como
esto:
--------default.conf----------
server {
listen 80;
server_name example.com www.example.com;

#Configura el directorio raíz servido públicamente
#Configura el archivo de índice para ser servido
root /var/www/example.com;
index index.html index.htm;

#Estas líneas crean un bypass para ciertos nombres de ruta #www.example.com/index.js ahora se enruta al puerto 3000. Para que funcione con nodejs
#instead of port 80
location /index.js {
proxy_pass http://localhost:3000;
proxy_set_header Host $host;
}
}
---------default.conf----------

pero así solo funciona con un dominio y en la configuración por defecto de nginx.
Así que nosotros debemos ver como están configurado los dominios y subdominios y hacérselo saber
al docker-compose.yml a través de su Dockerfile, explicándole donde debe copiar ese archivo:

/etc/nginx/sites-available/captionsconnection.com
/etc/nginx/sites-available/landingpage.captionsconnection.com

y posteriormente hacerle saber mediante un comando CMD, que haga el link de sites enabled

## PRODUCTION OR DEVELOPMENT

Una forma de gestionar NODE_ENV= development || production es con un script de .sh
llamado entrypoint.sh desde el Dockerfile. Ese entrypoint.sh contiene la lógica necesaria para dependiendo del comando que ejecutemos desde consola refiriendose al docker run, usará una configuración u otra:

> docker run Image_CC:v01 dev  
> docker run -it Image_CC:v01 dev /bin/bash
> @ CMD ["dev"] # development

    ...
    npm install
    	export NODE_ENV=development
    	export PORT=3333
    	exec npm run nodeDev
        "nodeDev": "cross-env NODE_ENV=development nodemon ./src/index.js"

> docker run Image_CC:v01 prod  
> docker run -it Image_CC:v01 prod /bin/bash
> @ CMD ["prod"] # production

    ...
    export NODE_ENV=production
    export PORT=6666
    exec npm run nodeProd
      "nodeProd": "SET NODE_ENV=production && pm2 start ./src/index.js"
