var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Persona = /** @class */ (function () {
    function Persona(_nombre, _apellido, _fechaNacimiento, _genero) {
        this.nombre = _nombre;
        this.apellido = _apellido;
        this.fechaNacimiento = _fechaNacimiento;
        this.genero = _genero;
    }
    Persona.prototype.edad = function () {
        var edad;
        edad = new Date().getFullYear() - this.fechaNacimiento.getFullYear();
        return edad;
    };
    Persona.prototype.getGenero = function () {
        if (this.genero == true) {
            return "masculino";
        }
        else {
            return "femenino";
        }
    };
    return Persona;
}());
var Estudiante = /** @class */ (function (_super) {
    __extends(Estudiante, _super);
    function Estudiante(_nombre, _apellido, _fechaNacimiento, _genero, _identificacion) {
        var _this = _super.call(this, _nombre, _apellido, _fechaNacimiento, _genero) || this;
        _this.materias = [];
        _this.notas = [];
        _this.identificacion = _identificacion;
        return _this;
    }
    Estudiante.prototype.agregarNota = function (_nuevaNota) {
        this.notas.push(_nuevaNota);
    };
    Estudiante.prototype.NotaMedia = function () {
        var notaMedia = 0;
        var coeficiente = this.notas.length;
        console.log(coeficiente);
        for (var i = 0; i < coeficiente; i++) {
            console.log(this.notas[i]);
            notaMedia += this.notas[i];
        }
        notaMedia = notaMedia / coeficiente;
        return notaMedia;
    };
    return Estudiante;
}(Persona));
var estudiante1 = new Estudiante("Juan", "Luna", new Date(23 - 09 - 91), true, "xrdee333gdfdfdfdf");
estudiante1.agregarNota(8.4);
estudiante1.agregarNota(8.4);
estudiante1.agregarNota(8.2);
estudiante1.agregarNota(8.3);
estudiante1.agregarNota(8.5);
estudiante1.agregarNota(8.6);
console.log("nombre: " + estudiante1.nombre + ", edad: " + estudiante1.edad() + " ,nota media: " + estudiante1.NotaMedia());
var persona1 = new Persona("Juan M.", "Luna Blanco", new Date(23 - 09 - 91), true);
console.log(persona1);
