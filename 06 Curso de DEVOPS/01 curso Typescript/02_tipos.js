// string
var variable1;
variable1 = "Pedro";
// number
var variable2;
variable2 = 48;
console.log(variable1 + 28);
console.log(28 + variable2);
// boolean
var sexo = true;
if (sexo == true) {
    console.log("es un hombre");
}
else {
    console.log("es una mujer");
}
// array
var edades = [1, 2, 3, 4, 67, 18];
console.log(edades);
// any
var cualquierDato;
cualquierDato = "Pepe";
console.log(cualquierDato);
cualquierDato = 2100;
console.log(cualquierDato);
cualquierDato = false;
console.log(cualquierDato);
// tuplas
var arrayTipos;
arrayTipos = ["Pollos", 543];
console.log(arrayTipos);
