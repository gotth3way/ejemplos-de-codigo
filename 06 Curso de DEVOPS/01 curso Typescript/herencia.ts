class BPersona {
    nombre: string;
    apellido: string;
    fechaNacimiento: Date;
    genero: boolean; // true = masculino 

    constructor (_nombre: string, _apellido: string, _fechaNacimiento: Date, _genero: boolean){
        this.nombre = _nombre;
        this.apellido = _apellido;
        this.fechaNacimiento = _fechaNacimiento;
        this.genero = _genero;
    }

    edad(){
        let edad: number;
        edad = new Date().getFullYear() - this.fechaNacimiento.getFullYear();
        return edad;
    }
    getGenero(){
        if (this.genero == true){
            return "masculino";
        }
         else {
             return "femenino";
         }
    }
}

class BEstudiante extends BPersona {
    identificacion: string;
    materias: string [] = [];
    notas: number [] = [];

    constructor( _nombre: string, _apellido: string, _fechaNacimiento: Date, _genero: boolean, 
        _identificacion: string){
        super(_nombre, _apellido, _fechaNacimiento, _genero);
        this.identificacion = _identificacion;
    }
    agregarNota(_nuevaNota: number){
        this.notas.push(_nuevaNota);
    }
    NotaMedia(){
        let notaMedia: number = 0;
        let coeficiente: number = this.notas.length;
        console.log(coeficiente);

        for(let i= 0; i < coeficiente; i++) {
            console.log(this.notas[i]);
            notaMedia += this.notas[i]; 
        }
        notaMedia = notaMedia / coeficiente; 
        return notaMedia;
    }
}

var estudiante1 : BEstudiante = new BEstudiante ("Juan", "Luna", new Date(23-09-91), true, "xrdee333gdfdfdfdf" );
estudiante1.agregarNota(8.4);
estudiante1.agregarNota(8.4);
estudiante1.agregarNota(8.2);
estudiante1.agregarNota(8.3);
estudiante1.agregarNota(8.5);
estudiante1.agregarNota(8.6);

console.log(`nombre: ${estudiante1.nombre}, edad: ${estudiante1.edad()} ,nota media: ${estudiante1.NotaMedia()}`);


var persona3 : BPersona = new BPersona ("Juan M.", "Luna Blanco", new Date(23-09-91), true);
console.log(persona3);