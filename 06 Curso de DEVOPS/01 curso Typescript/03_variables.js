// var i;
// function numerosConsecutivos() {
//     for ( i=1; i<=10; i++){
//         console.log(i);
//     }
//     console.log("por fuera del ciclo " + i);
// }
// numerosConsecutivos();
// console.log(i);
// var edad = 10;
// console.log(edad);
// if(edad > 5){
//     var edad: number = 5;
// }
var edad = 10;
console.log(edad); // 10
if (edad > 5) {
    var edad_1 = 5; // nunca se verá
    console.log("por dentro del bloque " + edad_1); // 5
}
console.log("por fuera del bloque " + edad); // 10
var PI = 3.1416;
console.log("Pi " + PI); // 10
