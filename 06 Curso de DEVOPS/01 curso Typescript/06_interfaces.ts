interface IPersona {
    nombre: string,
    apellido: string,
    edad: number
}

var personas : IPersona [] = [];

var personaX = {
    nombre: "Juan",
    apellido: "Luna",
    edad: 18
}



var persona2 = {
    nombre: "Pepe",
    apellido: "Gonzalez",
    edad: 37
};

personas.push(personaX);
personas.push(persona2);
console.log(personas);

for (let i=0; i< personas.length; i++){
    console.log(`nombre: ${personas[i].nombre}, apellido: ${personas[i].apellido} \n 
      edad: ${personas[i].edad}`);
}