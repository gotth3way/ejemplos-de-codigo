"use strict";
exports.__esModule = true;
exports.Persona = void 0;
var Persona = /** @class */ (function () {
    function Persona(_nombre, _apellido, _fechaNacimiento, _genero) {
        this.nombre = _nombre;
        this.apellido = _apellido;
        this.fechaNacimiento = _fechaNacimiento;
        this.genero = _genero;
    }
    Persona.prototype.edad = function () {
        var edad;
        edad = new Date().getFullYear() - this.fechaNacimiento.getFullYear();
        return edad;
    };
    Persona.prototype.getGenero = function () {
        if (this.genero == true) {
            return "masculino";
        }
        else {
            return "femenino";
        }
    };
    return Persona;
}());
exports.Persona = Persona;
var empleado = new Persona("John", "Moon", new Date(27 - 09 - 1971), true);
// console.log(empleado);
console.log(empleado.edad());
console.log(empleado.getGenero());
