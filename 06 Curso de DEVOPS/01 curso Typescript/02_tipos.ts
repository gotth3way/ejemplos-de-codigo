// string
var variable1: string;
variable1 = "Pedro";

// number
var variable2: number;
variable2 = 48;
console.log(variable1 + 28 );
console.log(28 + variable2);

// boolean
var sexo : boolean = true;
if(sexo == true){
    console.log("es un hombre");
} else {
    console.log("es una mujer");
}

// array
var edades : number[] = [1, 2, 3, 4, 67, 18];
console.log(edades);

// any
var cualquierDato : any;
cualquierDato = "Pepe";
console.log(cualquierDato);
cualquierDato = 2100;
console.log(cualquierDato);
cualquierDato = false;
console.log(cualquierDato);

// tuplas
var arrayTipos : [string, number];
arrayTipos = ["Pollos", 543];
console.log(arrayTipos);

