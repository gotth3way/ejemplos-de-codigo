// var sumar = function( param1, param2){
//     return param1 + param2;
// }

var sumar = (param1, param2) => param1 + param2;
console.log(sumar(8, 9));

var sumar10 = param1 => param1 + 10;
console.log(sumar10(8));

// var usuario = {
//     nombre: "Juan",
//     apellido: "Luna",
//     saludar: function() {
//         setTimeout(function(){
//             alert("Buenos días, " + this.nombre + " " + this.apellido); // Buenos días undefined undefined
//         }, 2000);
//     }
// };

// var usuario = {
//     nombre: "Juan",
//     apellido: "Luna",
//     saludar: ()=> {
//         setTimeout(function(){
//             alert("Buenos días, " + this.nombre + " " + this.apellido); // Buenos días undefined undefined
//         }, 2000);
//     }
// };

var usuario = {
    nombre: "Juan",
    apellido: "Luna",
    saludar: function() {
        setTimeout(()=>{  // solución callbacks funcion de flecha
            alert("Buenos días, " + this.nombre + " " + this.apellido); // Buenos días undefined undefined
        }, 2000);
    }
};

usuario.saludar();
