var personas = [];
var persona1 = {
    nombre: "Juan",
    apellido: "Luna",
    edad: 18
};
var persona2 = {
    nombre: "Pepe",
    apellido: "Gonzalez",
    edad: 37
};
personas.push(persona1);
personas.push(persona2);
console.log(personas);
for (var i = 0; i < personas.length; i++) {
    console.log("nombre: " + personas[i].nombre + ", apellido: " + personas[i].apellido + " \n \n      edad: " + personas[i].edad);
}
