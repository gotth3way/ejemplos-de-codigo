class APersona {
    nombre: string;
    apellido: string;
    fechaNacimiento: Date;
    genero: boolean; // true = masculino 

    constructor (_nombre: string, _apellido: string, _fechaNacimiento: Date, _genero: boolean){
        this.nombre = _nombre;
        this.apellido = _apellido;
        this.fechaNacimiento = _fechaNacimiento;
        this.genero = _genero;
    }

    edad(){
        let edad: number;
        edad = new Date().getFullYear() - this.fechaNacimiento.getFullYear();
        return edad;
    }
    getGenero(){
        if (this.genero == true){
            return "masculino";
        }
         else {
             return "femenino";
         }
    }
}

var empleado: APersona = new Persona("John", "Moon", new Date(27-09-1971), true);
// console.log(empleado);
console.log(empleado.edad());
console.log(empleado.getGenero());

